<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\PostController;


Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//This route is for logging out.
Route::get('/logout', [PostController::class, 'logout']);


// This route is for creation of a new post.
Route::get('/posts/create', [PostController::class, 'createPost']);

//This route is for saving the post on our database:
Route::post('/posts', [PostController::class, 'savePost']);

//This route is for the list of post on our database:
Route::get('/posts', [PostController::class, 'showPosts']);

//Define a route that will return a view containing only the authenticated user's post
Route::get('myPosts', [PostController::class, 'myPosts']);

//Define a route wherein a view showing a specific post with matching URL parameter ID will be returned to the user
Route::get('/posts/{id}', [PostController::class, 'show']);

//Define a route for editing a post
Route::get('/posts/{id}/edit', [PostController::class, 'edit'])->name('post.edit');

//Route for updating a post
Route::put('/posts/{id}', [PostController::class, 'update'])->name('post.update');
Route::put('/posts/{id}', [PostController::class, 'update'])->name('post.update');

// Route to archive a post
Route::delete('/posts/{id}', [PostController::class, 'archive'])->name('post.archive');
