@extends('layouts.app')

@section('tabName')
    {{$post->title}}
@endsection


@section('content')
    <div class="card col-6 mx-auto">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
                <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
                <p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
                <p class="card-subtitle text-muted mb-3">Updated at: {{$post->updated_at}}</p>
            <h4>Content:</h4>
                <p class="card-text">{{$post->body}}</p>
                <a href="/posts" class="card-link">View all posts</a>
        </div>
    </div>



@endsection
