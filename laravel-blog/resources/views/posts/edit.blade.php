@extends('layouts.app')

@section('tabName')
    Edit post
@endsection


@section('content')
    <form class="col-10" action="{{ route('post.update', ['id' => $post->id]) }}" method="POST">
            @csrf
            @method('PUT')
            <div class = "form-group">
    </form>
@endsection
